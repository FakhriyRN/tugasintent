package com.example.loginapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.loginapp.views.CustomView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CustomView customView =  new CustomView(this);
    }
}
