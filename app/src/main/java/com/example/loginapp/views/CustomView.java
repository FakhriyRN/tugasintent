package com.example.loginapp.views;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

public class CustomView extends View {

    private static final int SQUARE_SIZE = 300;
    private Rect mRectSquare;
    private Paint mPaintSquare;

    private Paint mPaintCircle;

    public CustomView(Context context) {
        super(context);

        init(null);
    }

    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(attrs);
    }

    public CustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(attrs);
    }

    public CustomView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init(attrs);
    }

    private void init(@Nullable AttributeSet set){
        mRectSquare = new Rect();
        mPaintSquare = new Paint(Paint.ANTI_ALIAS_FLAG);

        mPaintCircle = new Paint(Paint.ANTI_ALIAS_FLAG);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //Kepala
        mRectSquare.left = 100;
        mRectSquare.top = 80;
        mRectSquare.bottom = mRectSquare.top + SQUARE_SIZE;
        mRectSquare.right = mRectSquare.left + SQUARE_SIZE;


        mPaintSquare.setColor(Color.YELLOW);

        canvas.drawRect(mRectSquare, mPaintSquare);

        float cx, cy;
        float radius = 30f;

        //buat Mata Kiri
        cx = mRectSquare.left + 80f;
        cy = mRectSquare.top + 60f;

        mPaintCircle.setColor(Color.BLACK);

        canvas.drawCircle(cx, cy, radius, mPaintCircle);

        //buat Mata Kanan
        cx = mRectSquare.right - 80f;
        cy = mRectSquare.top + 60f;

        mPaintCircle.setColor(Color.BLACK);

        canvas.drawCircle(cx, cy, radius, mPaintCircle);

        //Antena
        mRectSquare.left = 100;
        mRectSquare.top = 20;
        mRectSquare.bottom = mRectSquare.top + 80;
        mRectSquare.right = mRectSquare.left + 50;


        mPaintSquare.setColor(Color.YELLOW);

        canvas.drawRect(mRectSquare, mPaintSquare);

        mRectSquare.right = 400;
        mRectSquare.top = 20;
        mRectSquare.bottom = mRectSquare.top + 80;
        mRectSquare.left = mRectSquare.right - 50;


        mPaintSquare.setColor(Color.YELLOW);

        canvas.drawRect(mRectSquare, mPaintSquare);

        //Badan
        mRectSquare.left = 100;
        mRectSquare.top = 230;
        mRectSquare.bottom = mRectSquare.top + 170;
        mRectSquare.right = mRectSquare.left + SQUARE_SIZE;


        mPaintSquare.setColor(Color.YELLOW);

        canvas.drawRect(mRectSquare, mPaintSquare);

        //Tangan Kiri
        mRectSquare.left = 10;
        mRectSquare.top = 230;
        mRectSquare.bottom = mRectSquare.top + 170;
        mRectSquare.right = mRectSquare.left + 80;


        mPaintSquare.setColor(Color.YELLOW);

        canvas.drawRect(mRectSquare, mPaintSquare);

        //Tangan Kanan
        mRectSquare.left = 410;
        mRectSquare.top = 230;
        mRectSquare.bottom = mRectSquare.top + 170;
        mRectSquare.right = mRectSquare.left + 80;


        mPaintSquare.setColor(Color.YELLOW);

        canvas.drawRect(mRectSquare, mPaintSquare);

        //Kaki Kiri
        mRectSquare.left = 100;
        mRectSquare.top = 410;
        mRectSquare.bottom = mRectSquare.top + 80;
        mRectSquare.right = mRectSquare.left + (SQUARE_SIZE / 2) - 10;


        mPaintSquare.setColor(Color.YELLOW);

        canvas.drawRect(mRectSquare, mPaintSquare);

        //Kaki Kanan
        mRectSquare.right = 400;
        mRectSquare.top = 410;
        mRectSquare.bottom = mRectSquare.top + 80;
        mRectSquare.left = mRectSquare.right - (SQUARE_SIZE / 2) + 10;


        mPaintSquare.setColor(Color.YELLOW);

        canvas.drawRect(mRectSquare, mPaintSquare);


    }
}
